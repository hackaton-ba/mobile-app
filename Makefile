#!/usr/bin/make -f

# RUN WHOLE PROCESS IN ONE SHELL
.ONESHELL:

# BY DEFAULT SHOW HELP
.DEFAULT_GOAL:=help
.DEFAULT:
	@$(MAKE) help

################################################################################
################################################################################
# START OF INIT BLOCK
################################################################################

# Are we running in an interactive shell? If so then we can use codes for
# a colored output
ifeq ("$(shell [ $(DOCKER_TTY) 0 ] && echo yes)","yes")
FORMAT_BOLD=\e[1m
FORMAT_RED=\033[0;31m
FORMAT_YELLOW=\033[0;33m
FORMAT_GREEN=\x1b[32;01m
FORMAT_RESET=\033[0m	
else
FORMAT_BOLD=
FORMAT_RED=
FORMAT_YELLOW=
FORMAT_GREEN=
FORMAT_RESET=
endif

# Path to the echo binary. This is needed because on some systems there are
# multiple versions installed and the alias "echo" may reffer to something
# different.
ECHO:=$(shell which echo)
OSECHOFLAG:=-e
UNAME_S:=$(shell uname -s)
XDEBUG_OPTIONS:=-dxdebug.remote_autostart=1
PWD:=$(shell pwd)
ifeq ($(UNAME_S),Darwin)
	ECHO=echo
	OSECHOFLAG=
	FORMAT_BOLD=
endif

# Color output based on CI variable
ANSI:=--no-ansi
DOCKER_TTY:=-T
ifeq ($(CI),)
	ANSI:=--ansi
	DOCKER_TTY:=
endif

# Preload .env file
ifneq ($(wildcard .env),)
    include ./.env
    export $(shell sed 's/=.*//' .env)
endif

################################################################################
# END OF INIT BLOCK
################################################################################
################################################################################


##@ DEVELOPMENT
# generate: ## Init the project
# 	fvm flutter pub run build_runner build --delete-conflicting-outputs

watch: ## Init the project
	fvm flutter pub run build_runner watch --delete-conflicting-outputs

run:
	fvm flutter run --flavor dev -d 2109119DG -t lib/main-dev.dart  

fodsqa:
	# fvm flutter run --flavor dev -d 2109119DG -t lib/main-dev.dart
	fvm flutter run -d 2109119DG -t lib/main.dart


help:
	@$(ECHO) $(OSECHOFLAG) "\n\n$(FORMAT_YELLOW)$(shell cat docs/ascii-logo.txt)$(FORMAT_RESET)"
	@awk 'BEGIN {FS = ":.*##";} /^[a-zA-Z_\-\/]+:.*?##/ { printf "  $(FORMAT_YELLOW)make %-20s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\n\033[1m%s\033[0m\n\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
	@$(ECHO) $(OSECHOFLAG) "\n\n"
